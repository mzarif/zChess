package main

import (
	"codeberg.org/mzarif/zChess/src"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
)

func main() {
	var a = app.New()
	var w = a.NewWindow("zChess")

	w.SetContent(src.NewBoard(src.StartPosition))
	w.SetPadded(false)
	w.Resize(fyne.NewSize(8 * src.TileSize, 8 * src.TileSize))
	w.SetFixedSize(true)
	w.CenterOnScreen()
	w.RequestFocus()
	w.ShowAndRun()
}
