/*
This file defines functions to generate moves for a given piece
A move will be represented as one byte with the following properties:
  - bits 0-2: x coordinate
  - bits 3-5: y coordinate
  - bits 6-7: flags for en passant and castling
*/
package src

const castle	int = 0x40
const enpassant int = 0x80

var yCoordinates = [8]int{0x0, 0x8, 0x10, 0x18, 0x20, 0x28, 0x30, 0x38}

func newMove(flag, y, x int) int8 {
	return int8(flag | y | x)
}

func getFlag(move int8) int {
	return int(move) & 0xc0
}

func pawnMoves(pieces []int8, x, y int) []int8 {
	var moves = []int8{}
	var color = getColor(pieces[getArrIndex(x, y)])
	var direction = getPawnDirection(color)

	if pieces[getArrIndex(x, y + direction)] == 0 { // single push
		moves = append(moves, newMove(0, yCoordinates[y + direction], x))
		if !hasMoved(pieces[getArrIndex(x, y)]) && pieces[getArrIndex(x, y + direction * 2)] == 0 { // double push
			moves = append(moves, newMove(0, yCoordinates[y + direction * 2], x))
		}
	}
	if x - 1 >= 0 && isEnemy(pieces[getArrIndex(x - 1, y + direction)], color) { // attack left
		moves = append(moves, newMove(0, yCoordinates[y + direction], x - 1))
	}
	if x + 1 <= 7 && isEnemy(pieces[getArrIndex(x + 1, y + direction)], color) { // attack right
		moves = append(moves, newMove(0, yCoordinates[y + direction], x + 1))
	}

	return moves
}

func knightMoves(pieces []int8, x, y int) []int8 {
	var moves = []int8{}
	var color = getColor(pieces[getArrIndex(x, y)])

	if x - 2 >= 0 && y - 1 >= 0 && isNotTeammate(pieces[getArrIndex(x - 2, y - 1)], color) {
		moves = append(moves, newMove(0, yCoordinates[y - 1], x - 2))
	}
	if x - 1 >= 0 && y - 2 >= 0 && isNotTeammate(pieces[getArrIndex(x - 1, y - 2)], color) {
		moves = append(moves, newMove(0, yCoordinates[y - 2], x - 1))
	}
	if x + 1 <= 7 && y - 2 >= 0 && isNotTeammate(pieces[getArrIndex(x + 1, y - 2)], color) {
		moves = append(moves, newMove(0, yCoordinates[y - 2], x + 1))
	}
	if x + 2 <= 7 && y - 1 >= 0 && isNotTeammate(pieces[getArrIndex(x + 2, y - 1)], color) {
		moves = append(moves, newMove(0, yCoordinates[y - 1], x + 2))
	}
	if x + 2 <= 7 && y + 1 <= 7 && isNotTeammate(pieces[getArrIndex(x + 2, y + 1)], color) {
		moves = append(moves, newMove(0, yCoordinates[y + 1], x + 2))
	}
	if x + 1 <= 7 && y + 2 <= 7 && isNotTeammate(pieces[getArrIndex(x + 1, y + 2)], color) {
		moves = append(moves, newMove(0, yCoordinates[y + 2], x + 1))
	}
	if x - 1 >= 0 && y + 2 <= 7 && isNotTeammate(pieces[getArrIndex(x - 1, y + 2)], color) {
		moves = append(moves, newMove(0, yCoordinates[y + 2], x - 1))
	}
	if x - 2 >= 0 && y + 1 <= 7 && isNotTeammate(pieces[getArrIndex(x - 2, y + 1)], color) {
		moves = append(moves, newMove(0, yCoordinates[y + 1], x - 2))
	}

	return moves
}

func bishopMoves(pieces []int8, x, y int) []int8 {
	var moves = []int8{}
	var color = getColor(pieces[getArrIndex(x, y)])
	var res int

	for i, j := x - 1, y - 1; i >= 0 && j >= 0; i, j = i - 1, j - 1 { // upper left
		res = inspectPiece(pieces[getArrIndex(i, j)], color)
		if res >= 0 {
			moves = append(moves, newMove(0, yCoordinates[j], i))
		}
		if res <= 0 {
			break
		}
	}
	for i, j := x + 1, y - 1; i <= 7 && j >= 0; i, j = i + 1, j - 1 { // upper right
		res = inspectPiece(pieces[getArrIndex(i, j)], color)
		if res >= 0 {
			moves = append(moves, newMove(0, yCoordinates[j], i))
		}
		if res <= 0 {
			break
		}
	}
	for i, j := x + 1, y + 1; i <= 7 && j <= 7; i, j = i + 1, j + 1 { // bottom right
		res = inspectPiece(pieces[getArrIndex(i, j)], color)
		if res >= 0 {
			moves = append(moves, newMove(0, yCoordinates[j], i))
		}
		if res <= 0 {
			break
		}
	}
	for i, j := x - 1, y + 1; i >= 0 && j <= 7; i, j = i - 1, j + 1 { // bottom left
		res = inspectPiece(pieces[getArrIndex(i, j)], color)
		if res >= 0 {
			moves = append(moves, newMove(0, yCoordinates[j], i))
		}
		if res <= 0 {
			break
		}
	}

	return moves
}

func rookMoves(pieces []int8, x, y int) []int8 {
	var moves = []int8{}
	var color = getColor(pieces[getArrIndex(x, y)])
	var res int

	for i := x - 1; i >= 0; i-- { // left
		res = inspectPiece(pieces[getArrIndex(i, y)], color)
		if res >= 0 {
			moves = append(moves, newMove(0, yCoordinates[y], i))
		}
		if res <= 0 {
			break
		}
	}
	for i := y - 1; i >= 0; i-- { // up
		res = inspectPiece(pieces[getArrIndex(x, i)], color)
		if res >= 0 {
			moves = append(moves, newMove(0, yCoordinates[i], x))
		}
		if res <= 0 {
			break
		}
	}
	for i := x + 1; i <= 7; i++ { // right
		res = inspectPiece(pieces[getArrIndex(i, y)], color)
		if res >= 0 {
			moves = append(moves, newMove(0, yCoordinates[y], i))
		}
		if res <= 0 {
			break
		}
	}
	for i := y + 1; i <= 7; i++ { // down
		res = inspectPiece(pieces[getArrIndex(x, i)], color)
		if res >= 0 {
			moves = append(moves, newMove(0, yCoordinates[i], x))
		}
		if res <= 0 {
			break
		}
	}

	return moves
}

func kingMoves(pieces []int8, x, y int) []int8 {
	var moves = []int8{}
	var color = getColor(pieces[getArrIndex(x, y)])
	
	if x - 1 >= 0 && y - 1 >= 0 && isNotTeammate(pieces[getArrIndex(x - 1, y - 1)], color) {
		moves = append(moves, newMove(0, yCoordinates[y - 1], x - 1))
	}
	if y - 1 >= 0 && isNotTeammate(pieces[getArrIndex(x, y - 1)], color) {
		moves = append(moves, newMove(0, yCoordinates[y - 1], x))
	}
	if x + 1 <= 7 && y - 1 >= 0 && isNotTeammate(pieces[getArrIndex(x + 1, y - 1)], color) {
		moves = append(moves, newMove(0, yCoordinates[y - 1], x + 1))
	}
	if x + 1 <= 7 && isNotTeammate(pieces[getArrIndex(x + 1, y)], color) {
		moves = append(moves, newMove(0, yCoordinates[y], x + 1))
	}
	if x + 1 <= 7 && y + 1 <= 7 && isNotTeammate(pieces[getArrIndex(x + 1, y + 1)], color) {
		moves = append(moves, newMove(0, yCoordinates[y + 1], x + 1))
	}
	if y + 1 <= 7 && isNotTeammate(pieces[getArrIndex(x, y + 1)], color) {
		moves = append(moves, newMove(0, yCoordinates[y + 1], x))
	}
	if x - 1 >= 0 && y + 1 <= 7 && isNotTeammate(pieces[getArrIndex(x - 1, y + 1)], color) {
		moves = append(moves, newMove(0, yCoordinates[y + 1], x - 1))
	}
	if x - 1 >= 0 && isNotTeammate(pieces[getArrIndex(x - 1, y)], color) {
		moves = append(moves, newMove(0, yCoordinates[y], x - 1))
	}

	if !hasMoved(pieces[getArrIndex(x, y)]) && !isUnderAttack(pieces, x, y, color) {
		if !rookFulfillsCastleRequirements(pieces[getArrIndex(7, y)], color) {
			goto QUEENSIDECASTLE
		}
		for i := x + 1; i < 7; i++ { // check space between king and rook
			if pieces[getArrIndex(i, y)] != 0 || isUnderAttack(pieces, i, y, color) {
				goto QUEENSIDECASTLE
			}
		}
		moves = append(moves, newMove(castle, yCoordinates[y], 6))
		QUEENSIDECASTLE:
		if !rookFulfillsCastleRequirements(pieces[getArrIndex(0, y)], color) {
			return moves
		}
		for i := x - 1; i > 1; i-- { // check space between king and rook
			if pieces[getArrIndex(i, y)] != 0 || isUnderAttack(pieces, i, y, color) {
				return moves
			}
		}
		moves = append(moves, newMove(castle, yCoordinates[y], 2))
	}

	return moves
}

func getPawnDirection(color int) int {
	if color == white {
		return -1
	}
	return 1
}

func isEnemy(enemyInQuestion int8, color int) bool {
	return enemyInQuestion != 0 && getColor(enemyInQuestion) != color
}

func isNotTeammate(teammateInQuestion int8, color int) bool {
	return getColor(teammateInQuestion) != color
}

func inspectPiece(piece int8, color int) int {
	if piece == 0 { // empty tile
		return 1
	} else if getColor(piece) != color { // enemy
		return 0
	} else { // teammate
		return -1
	}
}

func rookFulfillsCastleRequirements(piece int8, color int) bool {
	return getType(piece) == rook && !hasMoved(piece) && getColor(piece) == color
}
