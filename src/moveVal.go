/*
This file defines functions for move validation
*/
package src

import (
	"math"
	"sync"
)

// scans for enemy pieces starting from the x and y coordinates
func isUnderAttack(pieces []int8, x, y, color int) bool {
	var threat = false
	var wg = sync.WaitGroup{}

	wg.Add(3)
	go checkTop(pieces, x, y, color, &threat, &wg)
	go checkBottom(pieces, x, y, color, &threat, &wg)
	go checkForKnights(pieces, x, y, color, &threat, &wg)
	wg.Wait()

	return threat
}

func checkTop(pieces []int8, x, y, color int, threat *bool, wg *sync.WaitGroup) {
	defer wg.Done()
	var piece int8
	
	for i := x - 1; i >= 0; i-- { // left
		if piece = pieces[getArrIndex(i, y)]; getColor(piece) == color {
			break
		} else if canAttack(i, y, x, y, getType(piece), getColor(piece)) {
			*threat = true
			return
		}
	}
	for i, j := x - 1, y - 1; i >= 0 && j >= 0; i, j = i - 1, j - 1{ // upper left
		if piece = pieces[getArrIndex(i, j)]; getColor(piece) == color {
			break
		} else if canAttack(i, j, x, y, getType(piece), getColor(piece)) {
			*threat = true
			return
		}
	}
	for i := y - 1; i >= 0; i-- { // up
		if piece = pieces[getArrIndex(x, i)]; getColor(piece) == color {
			break
		} else if canAttack(x, i, x, y, getType(piece), getColor(piece)) {
			*threat = true
			return
		}
	}
	for i, j := x + 1, y - 1; i <= 7 && j >= 0; i, j = i + 1, j - 1 { // upper right
		if piece = pieces[getArrIndex(i, j)]; getColor(piece) == color {
			break
		} else if canAttack(i, j, x, y, getType(piece), getColor(piece)) {
			*threat = true
			return
		}
	}
}

func checkBottom(pieces []int8, x, y, color int, threat *bool, wg *sync.WaitGroup) {
	defer wg.Done()
	var piece int8
	
	for i := x + 1; i <= 7; i++ { // right
		if piece = pieces[getArrIndex(i, y)]; getColor(piece) == color {
			break
		} else if canAttack(i, y, x, y, getType(piece), getColor(piece)) {
			*threat = true
			return
		}
	}
	for i, j := x + 1, y + 1; i <= 7 && j <= 7; i, j = i + 1, j + 1{ // bottom right
		if piece = pieces[getArrIndex(i, j)]; getColor(piece) == color {
			break
		} else if canAttack(i, j, x, y, getType(piece), getColor(piece)) {
			*threat = true
			return
		}
	}
	for i := y + 1; i <= 7; i++ { // down
		if piece = pieces[getArrIndex(x, i)]; getColor(piece) == color {
			break
		} else if canAttack(x, i, x, y, getType(piece), getColor(piece)) {
			*threat = true
			return
		}
	}
	for i, j := x - 1, y + 1; i >= 0 && j <= 7; i, j = i - 1, j + 1 { // bottom left
		if piece = pieces[getArrIndex(i, j)]; getColor(piece) == color {
			break
		} else if canAttack(i, j, x, y, getType(piece), getColor(piece)) {
			*threat = true
			return
		}
	}
}

func checkForKnights(pieces []int8, x, y, color int, threat *bool, wg *sync.WaitGroup) {
	defer wg.Done()

	if x - 2 >= 0 && y - 1 >= 0 && isEnemyKnight(pieces[getArrIndex(x - 2, y - 1)], color) {
		*threat = true
	} else if x - 1 >= 0 && y - 2 >= 0 && isEnemyKnight(pieces[getArrIndex(x - 1, y - 2)], color) {
		*threat = true
	} else if x + 1 <= 7 && y - 2 >= 0 && isEnemyKnight(pieces[getArrIndex(x + 1, y - 2)], color) {
		*threat = true
	} else if x + 2 <= 7 && y - 1 >= 0 && isEnemyKnight(pieces[getArrIndex(x + 2, y - 1)], color) {
		*threat = true
	} else if x + 2 <= 7 && y + 1 <= 7 && isEnemyKnight(pieces[getArrIndex(x + 2, y + 1)], color) {
		*threat = true
	} else if x + 1 <= 7 && y + 2 <= 7 && isEnemyKnight(pieces[getArrIndex(x + 1, y + 2)], color) {
		*threat = true
	} else if x - 1 >= 0 && y + 2 <= 7 && isEnemyKnight(pieces[getArrIndex(x - 1, y + 2)], color) {
		*threat = true
	} else if x - 2 >= 0 && y + 1 <= 7 && isEnemyKnight(pieces[getArrIndex(x - 2, y + 1)], color) {
		*threat = true
	}
}

func canAttack(x, y, enemyX, enemyY, pieceType, color int) bool {
	switch pieceType {
	case pawn:		return y + getPawnDirection(color) == enemyY && math.Abs(float64(x - enemyX)) == 1
	case bishop:	return math.Abs(float64(x - enemyX)) == math.Abs(float64(y - enemyY))
	case rook:		return x == enemyX || y == enemyY
	case queen:		return (math.Abs(float64(x - enemyX)) == math.Abs(float64(y - enemyY))) || (x == enemyX || y == enemyY)
	case king:		return math.Abs(float64(x - enemyX)) <= 1 && math.Abs(float64(y - enemyY)) <= 1
	default:		return false
	}
}

func isEnemyKnight(piece int8, color int) bool {
	return getType(piece) == knight && getColor(piece) != color
}
