package src

import (
	"fmt"
	"unicode"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

type GameState int8
const (
	selectPiece GameState = iota
	movePiece	GameState = iota
)

type Board struct {
	widget.BaseWidget
	pieces 				[]int8
	moves				[]int8 // stores generated moves of the selectedPiece
	chosenMove			int // index to a single move in moves (used for the renderer)
	turn				int
	selectedPiecePos	int8
	wKingPos			int8
	bKingPos			int8
	state				GameState
	winnerDecided		bool
}

const StartPosition string = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR"

func NewBoard(fenCode string) *Board {
	var b = &Board {
		pieces			: make([]int8, 64),
		chosenMove		: -1,
		turn			: white,
		wKingPos		: 0x3c,
		bKingPos		: 0x4,
		state			: selectPiece,
		winnerDecided	: false,
	}

	//fill pieces according to the fenCode
	var x, y int
	for _, c := range fenCode {
		if unicode.IsDigit(c) {
			x += int(c)
		} else if c == '/' {
			x = 0
			y++
		} else {
			switch c {
			case 'p': b.pieces[getArrIndex(x, y)] = newPiece(black, pawn)
			case 'P': b.pieces[getArrIndex(x, y)] = newPiece(white, pawn)
			case 'n': b.pieces[getArrIndex(x, y)] = newPiece(black, knight)
			case 'N': b.pieces[getArrIndex(x, y)] = newPiece(white, knight)
			case 'b': b.pieces[getArrIndex(x, y)] = newPiece(black, bishop)
			case 'B': b.pieces[getArrIndex(x, y)] = newPiece(white, bishop)
			case 'r': b.pieces[getArrIndex(x, y)] = newPiece(black, rook)
			case 'R': b.pieces[getArrIndex(x, y)] = newPiece(white, rook)
			case 'q': b.pieces[getArrIndex(x, y)] = newPiece(black, queen)
			case 'Q': b.pieces[getArrIndex(x, y)] = newPiece(white, queen)
			case 'k': b.pieces[getArrIndex(x, y)] = newPiece(black, king)
			case 'K': b.pieces[getArrIndex(x, y)] = newPiece(white, king)
			}
			x++
		}
	}

	b.ExtendBaseWidget(b)
	return b
}

func (b *Board) CreateRenderer() fyne.WidgetRenderer {
	return newBoardRenderer(b)
}

func (b *Board) Tapped(e *fyne.PointEvent) {
	if b.winnerDecided {
		return
	}
	var x, y = int(e.Position.X / TileSize), int(e.Position.Y / TileSize)

	if b.state == selectPiece {
		if getColor(b.pieces[getArrIndex(x, y)]) != b.turn {
			b.moves = nil
			return
		}

		switch getType(b.pieces[getArrIndex(x, y)]) {
		case pawn:		b.moves = pawnMoves(b.pieces, x, y)
		case knight:	b.moves = knightMoves(b.pieces, x, y)
		case bishop:	b.moves = bishopMoves(b.pieces, x, y)
		case rook:		b.moves = rookMoves(b.pieces, x, y)
		case queen:		b.moves = append(bishopMoves(b.pieces, x, y), rookMoves(b.pieces, x, y)...)
		case king:		b.moves = kingMoves(b.pieces, x, y)
		}

		b.selectedPiecePos = int8((y << 3) | x)
		b.filterMoves()
		if len(b.moves) != 0 {
			b.Refresh()
			b.state = movePiece
		}
	} else {
		var xY = int8((y << 3) | x)
		for i, m := range b.moves {
			if xY == m & 0x3f { // mask away the flags
				b.chosenMove = i
				var currX, currY = getXYCoords(b.selectedPiecePos)
				b.makeMove(currX, currY, x, y, getFlag(m))
				setMoved(&b.pieces[getArrIndex(x, y)])

				if getType(b.pieces[getArrIndex(x, y)]) == king {
					if getColor(b.pieces[getArrIndex(x, y)]) == white {
						b.wKingPos = xY
					} else {
						b.bKingPos = xY
					}
				}

				if b.turn == white {
					b.turn = black
				} else {
					b.turn = white
				}

				b.Refresh()
				if !b.checkForOpponentsLegalMoves() {
					var winner string
					if b.turn == white {
						winner = "Black"
					} else {
						winner = "White"
					}
					fmt.Println("Game Over!", winner, "wins!")
					b.winnerDecided = true
				}
				b.chosenMove = -1
				b.state = selectPiece
				return
			}
		}

		b.Refresh()
		b.chosenMove = -1
		b.state = selectPiece
	}
}

func getArrIndex(x, y int) int {
	return x + (y << 3)
}

func getXYCoords(i int8) (int, int) { // first int is x, second is y
	return int(i & 0x7), int((i >> 3) & 0x7)
}

func (b *Board) makeMove(oldX, oldY, newX, newY, flag int) int8 {
	var capturedPiece = b.pieces[getArrIndex(newX, newY)]
	b.pieces[getArrIndex(newX, newY)] = b.pieces[getArrIndex(oldX, oldY)]
	b.pieces[getArrIndex(oldX, oldY)] = 0

	if flag == castle {
		// move the rook
		var oldRookX, newRookX int
		if newX == 6 { // kingside
			oldRookX, newRookX = 7, 5
		} else { // queenside
			oldRookX, newRookX = 0, 3
		}
		b.pieces[getArrIndex(newRookX, newY)] = b.pieces[getArrIndex(oldRookX, oldY)]
		//setMoved(&b.pieces[getArrIndex(newRookX, newY)])
		b.pieces[getArrIndex(oldRookX, oldY)] = 0
	}

	return capturedPiece
}

func (b *Board) undoMove(oldX, oldY, newX, newY int, capturedPiece int8) {
	b.pieces[getArrIndex(oldX, oldY)] = b.pieces[getArrIndex(newX, newY)]
	b.pieces[getArrIndex(newX, newY)] = capturedPiece
}

// filters out illegal moves by simulating every move on the board and then checking for king's safety
// TODO: set b.moves and b.selectedPiecePos accordingly before calling filterMoves
func (b *Board) filterMoves() {
	var legalMoves = []int8{}
	var currX, currY = getXYCoords(b.selectedPiecePos)
	var kingX, kingY int

	if b.turn == white {
		kingX, kingY = getXYCoords(b.wKingPos)
	} else {
		kingX, kingY = getXYCoords(b.bKingPos)
	}

	var isKing = false
	if kingX == currX && kingY == currY { // king is the selected piece
		isKing = true
	}

	for _, m := range b.moves {
		var flag = getFlag(m)
		if flag == castle { // we already checked king's safety in this case
			continue
		}
		var newX, newY = getXYCoords(m)
		var capturedPiece = b.makeMove(currX, currY, newX, newY, flag)
		var x, y int
		if isKing {
			x, y = newX, newY
		} else {
			x, y = kingX, kingY
		}
		if !isUnderAttack(b.pieces, x, y, b.turn) {
			legalMoves = append(legalMoves, m)
		}
		b.undoMove(currX, currY, newX, newY, capturedPiece)
	}

	if len(legalMoves) == 0 {
		b.moves = nil
	} else {
		b.moves = legalMoves
	}
}

// I did not come up with a better solution for checkmate detection than to loop around the board until I find a piece that still has legal moves
func (b *Board) checkForOpponentsLegalMoves() bool {
	// it's important in which direction we iterate through the array
	if b.turn == white {
		for y := 7; y >= 0; y-- {
			for x := 7; x >= 0; x-- {
				var piece = b.pieces[getArrIndex(x, y)]
				if getColor(piece) == b.turn {
					b.selectedPiecePos = int8((y << 3) | x)
					switch getType(piece) {
					case pawn: b.moves = pawnMoves(b.pieces, x, y)
					case knight: b.moves = knightMoves(b.pieces, x, y)
					case bishop: b.moves = bishopMoves(b.pieces, x, y)
					case rook: b.moves = rookMoves(b.pieces, x, y)
					case queen: b.moves = append(bishopMoves(b.pieces, x, y), rookMoves(b.pieces, x, y)...)
					case king: b.moves = kingMoves(b.pieces, x, y)
					}
					b.filterMoves()
					if len(b.moves) != 0 {
						return true
					}
				}
			}
		}
	} else {
		for y := 0; y <= 7; y++ {
			for x := 0; x <= 7; x++ {
				var piece = b.pieces[getArrIndex(x, y)]
				if getColor(piece) == b.turn {
					b.selectedPiecePos = int8((y << 3) | x)
					switch getType(piece) {
					case pawn: b.moves = pawnMoves(b.pieces, x, y)
					case knight: b.moves = knightMoves(b.pieces, x, y)
					case bishop: b.moves = bishopMoves(b.pieces, x, y)
					case rook: b.moves = rookMoves(b.pieces, x, y)
					case queen: b.moves = append(bishopMoves(b.pieces, x, y), rookMoves(b.pieces, x, y)...)
					case king: b.moves = kingMoves(b.pieces, x, y)
					}
					b.filterMoves()
					if len(b.moves) != 0 {
						return true
					}
				}
			}
		}
	}

	return false
}
