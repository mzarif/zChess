/*
a piece will be represented by one byte which has the following properties:
	- bits 0-2:	type
	- bits 3-4:	color
	- bit  5:	moved
because operations done with integers of native length is often more efficient than using single byte values,
so I decided to operate on normal ints and then store the end result in one byte
*/

package src

const pawn		int = 0x1
const knight	int = 0x2
const bishop	int = 0x3
const rook		int = 0x4
const queen		int = 0x5
const king		int = 0x6

const white int = 0x8
const black int = 0x10

func newPiece(color, pieceType int) int8 {
	return int8(color | pieceType)
}

func getType(piece int8) int {
	return int(piece) & 0x7
}

func getColor(piece int8) int {
	return int(piece) & 0x18
}

func hasMoved(piece int8) bool {
	return int(piece) & 0x20 != 0
}

func setMoved(piece *int8) {
	*piece |= 0x20
}
