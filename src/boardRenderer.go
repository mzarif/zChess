package src

import (
	"image/color"

	"codeberg.org/mzarif/zChess/img"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
)

const TileSize float32 = 50

type boardRenderer struct {
	board *Board
	/*
	A tile will be a container which itself can have a list of CanvasObjects
	I assume that this list will at least contain 1 and at most 3 objects in the following order:
		1. beige or brown rectangle
		2. blue rectangle to symbolize a possible move (optional)
		3. an image of a piece (optional)
	*/
	tiles []fyne.CanvasObject
}

func newBoardRenderer(b *Board) *boardRenderer {
	var tiles = make([]fyne.CanvasObject, 64)
	var rect *canvas.Rectangle 

	for y := 0; y < 8; y++ {
		for x := 0; x < 8; x++ {
			if (x + y) & 1 == 0 {
				rect = canvas.NewRectangle(color.RGBA{0xff, 0xde, 0xad, 0xff})
			} else {
				rect = canvas.NewRectangle(color.RGBA{0x8b, 0x45, 0x13, 0xff})
			}
			tiles[getArrIndex(x, y)] = container.NewStack(rect)
			initImage(tiles[getArrIndex(x, y)].(*fyne.Container), b.pieces[getArrIndex(x, y)])
		}
	}

	return &boardRenderer{b, tiles}
}

func (bR *boardRenderer) Layout(size fyne.Size) {
	var tileWidth = size.Width / 8
	var tileHeight = size.Height / 8
	var tile fyne.CanvasObject

	for y := 0; y < 8; y++ {
		for x := 0; x < 8; x++ {
			tile = bR.tiles[getArrIndex(x, y)]
			tile.Move(fyne.NewPos(float32(x) * tileWidth, float32(y) * tileHeight))
			tile.Resize(fyne.NewSize(tileWidth, tileHeight))
		}
	}
}

func (bR *boardRenderer) MinSize() fyne.Size {
	return fyne.NewSize(8 * TileSize, 8 * TileSize)
}

func (bR *boardRenderer) Refresh() {
	if bR.board.state == selectPiece { // render all the possible moves of the selected piece on the board
		for _, m := range bR.board.moves {
			var x, y = getXYCoords(m)
			var tile = bR.tiles[getArrIndex(x, y)].(*fyne.Container)
			var blueRect = canvas.NewRectangle(color.RGBA{0x00, 0x90, 0xff, 0xb3})

			// the order of canvasObjects in tile.Objects is important for rendering
			if len(tile.Objects) == 1 {
				tile.Objects = append(tile.Objects, blueRect)
			} else {
				tile.Objects = append(tile.Objects[:2], tile.Objects[1:]...)
				tile.Objects[1] = blueRect
			}
			tile.Refresh()
		}
	} else { // render the new position of the selected piece
		if bR.board.chosenMove != -1 {
			var oldX, oldY = getXYCoords(bR.board.selectedPiecePos)
			var newX, newY = getXYCoords(bR.board.moves[bR.board.chosenMove])
			var oldTile, newTile = bR.tiles[getArrIndex(oldX, oldY)].(*fyne.Container), bR.tiles[getArrIndex(newX, newY)].(*fyne.Container)
			var flag = getFlag(bR.board.moves[bR.board.chosenMove])

			// update images
			if (len(newTile.Objects) == 3) { // ensure that a tile has at most 3 objects
				newTile.Objects[2] = oldTile.Objects[1]
			} else {
				newTile.Add(oldTile.Objects[1])
			}
			oldTile.Objects = oldTile.Objects[:1]

			if flag == castle {
				var oldRookX, newRookX int
				if newX == 6 { // kingside
					oldRookX, newRookX = 7, 5
				} else { // queenside
					oldRookX, newRookX = 0, 3
				}
				// we know that the rook will always move to an empty tile during castling
				var oldRookTile, newRookTile = bR.tiles[getArrIndex(oldRookX, oldY)].(*fyne.Container), bR.tiles[getArrIndex(newRookX, newY)].(*fyne.Container)
				newRookTile.Add(oldRookTile.Objects[1])
				oldRookTile.Objects = oldRookTile.Objects[:1]
			}
		}

		// remove blue rectangles
		for _, m := range bR.board.moves {
			var x, y = getXYCoords(m)
			var tile = bR.tiles[getArrIndex(x, y)].(*fyne.Container)
			tile.Objects = append(tile.Objects[:1], tile.Objects[2:]...)
			tile.Refresh()
		}
	}
}

func (bR *boardRenderer) Objects() []fyne.CanvasObject {
	return bR.tiles
}

func (bR *boardRenderer) Destroy() {
	bR.tiles = nil // releases underlying array for garbage collection
}

func initImage(tile *fyne.Container, piece int8) {
	switch getType(piece) {
	case pawn:
		if getColor(piece) == white {
			tile.Add(canvas.NewImageFromResource(img.WPawnSvg))
		} else {
			tile.Add(canvas.NewImageFromResource(img.BPawnSvg))
		}
	case knight:
		if getColor(piece) == white {
			tile.Add(canvas.NewImageFromResource(img.WKnightSvg))
		} else {
			tile.Add(canvas.NewImageFromResource(img.BKnightSvg))
		}
	case bishop:
		if getColor(piece) == white {
			tile.Add(canvas.NewImageFromResource(img.WBishopSvg))
		} else {
			tile.Add(canvas.NewImageFromResource(img.BBishopSvg))
		}
	case rook:
		if getColor(piece) == white {
			tile.Add(canvas.NewImageFromResource(img.WRookSvg))
		} else {
			tile.Add(canvas.NewImageFromResource(img.BRookSvg))
		}
	case queen:
		if getColor(piece) == white {
			tile.Add(canvas.NewImageFromResource(img.WQueenSvg))
		} else {
			tile.Add(canvas.NewImageFromResource(img.BQueenSvg))
		}
	case king:
		if getColor(piece) == white {
			tile.Add(canvas.NewImageFromResource(img.WKingSvg))
		} else {
			tile.Add(canvas.NewImageFromResource(img.BKingSvg))
		}
	}
}
